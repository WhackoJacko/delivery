from django.conf.urls import url

from main.views import (
    weekly_menu_view, home_view, registration_view, login_view, logout_view,
    account_view, change_password_view, add_item_view, remove_item_view,
    add_address_view, add_delivery_time_view
)

urlpatterns = [
    url(r'^$', home_view, name='home'),
    url(r'^weekly_menu/$', weekly_menu_view, name='weekly_menu'),
    url(r'^registration/$', registration_view, name='registration'),
    url(r'^login/$', login_view, name='login'),
    url(r'^logout/$', logout_view, name='logout'),
    url(r'^account/$', account_view, name='account'),
    url(r'^change_password/$', change_password_view, name='change_password'),
    url(r'^add_item/$', add_item_view, name='add_item'),
    url(r'^remove_item/$', remove_item_view, name='remove_item'),
    url(r'^add_address/$', add_address_view, name='add_address'),
    url(r'^add_delivery_time/$', add_delivery_time_view, name='add_delivery_time'),
]

from django.conf import settings
from django.conf.urls import include, url

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
                      url(r'^__debug__/', include(debug_toolbar.urls)),
                  ] + urlpatterns
