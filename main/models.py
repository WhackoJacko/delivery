# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

from dishes.models import Dinner


class Client(models.Model):
    phone = models.CharField(verbose_name='Номер телефона', max_length=16,
                             unique=True)
    user = models.OneToOneField(User, verbose_name='Пользователь')


class SmsCode(models.Model):
    value = models.CharField(verbose_name='СМС код', max_length=4)
    created_at = models.DateTimeField(verbose_name='Создан', auto_now_add=True)
    client = models.ForeignKey(Client, verbose_name='Клиент')


class Basket(models.Model):
    client = models.ForeignKey(Client, null=True)
    address = models.CharField(max_length=512, null=True)
    delivery_at = models.DateTimeField(null=True)


class BasketItem(models.Model):
    basket = models.ForeignKey(Basket, related_name='items')
    dinner = models.ForeignKey(Dinner)
    quantity = models.IntegerField()


class Order(models.Model):
    client = models.ForeignKey(Client)
    address = models.CharField(max_length=512)
    delivery_at = models.DateTimeField()


class OrderItem(models.Model):
    order = models.ForeignKey(Order)
    dinner = models.ForeignKey(Dinner)
    quantity = models.IntegerField()
